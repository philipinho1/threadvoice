# ThreadVoice

ThreadVoice is a Twitter bot that turns Twitter threads into listenable audio podcasts. The Text-to-Speech engine is powered by Google, Microsoft, Amazon and VoiceRSS.  

## Setup
The appropriate credentials should be configured in the following files:  
`src/main/resources/twitter4j.properties`  
`src/main/resources/bot.properties`  

The `src/main/resources/threadvoicecloud.json` file should be replaced with the one you generate from Google Cloud Platform (TTS).  

Create an MYSQL database called "threadvoice" and upload the `schema.sql` database file.  

## To install the VoiceRSS library
Execute:  
```mvn install:install-file \
   -Dfile=libs/voicerss_tts.jar \
   -DgroupId=com.voicerss.tts \
   -DartifactId=voicerss \
   -Dversion=1.0 \
   -Dpackaging=jar \
   -DgeneratePom=true
```
   
## To compile the program
`mvn clean install`  

## To run the program
`java -jar target/threadvoice-2.0-SNAPSHOT.jar`  
Use `nohup` to keep the program running even after the terminal is closed:  
`nohup java -jar target/threadvoice-2.0-SNAPSHOT.jar &`  