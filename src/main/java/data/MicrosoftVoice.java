package data;

public class MicrosoftVoice {

    public static String englishJessaRUS = "en-US-JessaRUS";
    public static String englishJessaNeural = "en-US-JessaNeural";

    public static String englishZiraRUS = "en-US-ZiraRUS";
    public static String englishBenjaminRUS = "en-US-BenjaminRUS";

    public static String englishGuyRus = "en-US-Guy24kRUS";
    public static String englishGuyNeural = "en-US-GuyNeural";
    private static String englishHazelRUS = "en-GB-HazelRUS";

    public static String frenchHortenseRUS = "fr-FR-HortenseRUS";

    public static String germanHeddaRUS = "de-DE-HeddaRUS";

    public static String spanishHelenaRUS = "es-ES-HelenaRUS";

    public static String portugueseHeloisaRUS = "pt-BR-HeloisaRUS";

    public static String italianLuciaRUS = "it-IT-LuciaRUS";

    public static String russianEkaterinaRUS = "ru-RU-EkaterinaRUS";

    public static String turkishSedaRUS = "tr-TR-SedaRUS";

}
