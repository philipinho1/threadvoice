import com.vdurmont.emoji.EmojiParser;
import TTS_Engine.AmazonTTS;
import TTS_Engine.GoogleTTS;
import TTS_Engine.VoiceRSS;
import utils.DBHelper;
import utils.DetectLanguage;
import utils.ReadProperty;
import twitter4j.*;

import java.util.*;

import static utils.Utils.cleanThread;

public class Stream {
    private static final Twitter twitter = TwitterFactory.getSingleton();
    private static final TwitterStream twitterStream =
            new TwitterStreamFactory(twitter.getConfiguration()).getInstance();

    public static void main(String[] args) {

        String botUsername = ReadProperty.getValue("twitter.username");

        StatusListener listener = new StatusListener() {

            public void onStatus(Status tweet) {

                try {

                        long tweetReferencedId = -1;
                        long mentionTweetId = tweet.getId();

                        String threadLanguage = null;

                        if (tweet.getQuotedStatus() != null){
                            tweetReferencedId = tweet.getQuotedStatusId();

                        } else if (tweet.getInReplyToStatusId() != -1){
                            tweetReferencedId = tweet.getInReplyToStatusId();
                        }

                    if (tweetReferencedId != -1) {

                        String whoMentionedMe = tweet.getUser().getScreenName();

                        String outputFilename = null;

                        List<Status> tweetSince = ThreadReader.getTweetSinceId(tweetReferencedId);
                        List<Status> formatTweets = ThreadReader.formatThread(tweetSince);

                        StringBuilder threadContent = new StringBuilder();

                        if (formatTweets != null && formatTweets.size() >= 2) {
                            long firstTweetId = formatTweets.get(0).getId();
                            String threadAuthor = formatTweets.get(0).getUser().getScreenName();
                            String voiceFilename = threadAuthor + "_" + firstTweetId;

                            boolean threadExist = DBHelper.isThreadExists(firstTweetId);

                            if (threadExist && !tweet.getText().toLowerCase().contains("refresh")) {
                                outputFilename = DBHelper.getThreadVoiceName(firstTweetId);

                            } else {

                                for (Status tweets : formatTweets) {
                                    threadContent.append(cleanThread(tweets.getText())).append("\n\n");
                                }

                                String threadSnippet = threadContent.toString().substring(0, Math.min(700, threadContent.toString().length()));
                                threadLanguage = DetectLanguage.getLanguage(threadSnippet);

                                //Detect language
                                if (!DetectLanguage.isSupported(threadLanguage)) {
                                    String notSupportedMessage = "Sorry, it is either the language on this thread is not yet supported was not detected.";

                                    //System.out.println(notSupportedMessage);
                                    replyTweet( "@"+ tweet.getUser().getScreenName()+ " " + notSupportedMessage, mentionTweetId);

                                } else {

                                    threadContent.append("\n").append(credits(threadLanguage,EmojiParser.removeAllEmojis(formatTweets.get(0).getUser().getName())));
                                  //  threadContent.append("\n\n").append("Voice Transcription by ThreadVoice.net.");

                                    if (threadContent.length() < 2995) {
                                        outputFilename = AmazonTTS.processSpeech(threadContent.toString(), threadLanguage, voiceFilename);
                                    } else if (threadContent.length() < 4995) {
                                        outputFilename = GoogleTTS.processSpeech(threadContent.toString(), threadLanguage, voiceFilename);
                                    } else {
                                        outputFilename = VoiceRSS.processSpeech(threadContent.toString(), threadLanguage, voiceFilename);
                                    }
                                }
                            }

                            if (outputFilename != null) {
                                String audioLink = ReadProperty.getValue("filepath") + outputFilename;
                                replyTweet(tweetMessage(whoMentionedMe, threadAuthor, audioLink), tweet.getId());

                                //save if thread doesn't exist
                                if (!threadExist) {
                                    String firstTweet = threadContent.substring(0, Math.min(280, threadContent.toString().length()));
                                    DBHelper.saveTweet(whoMentionedMe, mentionTweetId, firstTweetId, outputFilename, threadAuthor, firstTweet, threadLanguage);
                                }
                            } else {
                                //  String response= "Sorry, something went wrong while trying to process the audio. ";
                                //  replyTweet( "@"+ whoMentionedMe+ " " + response, mentionTweetId);
                            }
                        } else {
                            //   String response= "Sorry, it is either this thread has less than 2 tweets or too old to be processed.";
                           //    replyTweet( "@"+ whoMentionedMe+ " " + response, mentionTweetId);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

            }

            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {

            }

            public void onScrubGeo(long userId, long upToStatusId) {

            }

            @Override
            public void onStallWarning(StallWarning stallWarning) {

            }

            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };

        FilterQuery fq = new FilterQuery();
        String[] keywords = {botUsername};

        fq.track(keywords);

        twitterStream.addListener(listener);
        twitterStream.filter(fq);
    }

    private static String tweetMessage(String username, String threadAuthor, String audioLink){
        String userMention = "@" + username;
        String[] messages = {"Yes! This thread has been converted to voice version. You can listen or download it via ", "Hey, thread from @" +threadAuthor +  " is ready in audio format. You can listen or download it via: "};
        int rand = (int)(messages.length * Math.random());

        return userMention + " " + messages[rand] + " " + audioLink;
    }

    private static void replyTweet(String text, long id) {
        try {
            Status status = twitter.updateStatus(
                    new StatusUpdate(text)
                            .inReplyToStatusId(id));

            System.out.println("Replied to: " + status.getInReplyToScreenName());

        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    private static String credits(String language, String authorName){
        switch (language){

            case "fr": return "Écrit par " + authorName +".\n" +
                    "Audio propulsé par " + ReadProperty.getValue("site.domain");

            case "es": return "Escrito por "+ authorName +".\n" +
                    "Audio impulsado por " + ReadProperty.getValue("site.domain");

            case "pt": return "Escrito por " + authorName +".\n" +
                    "Áudio desenvolvido por " + ReadProperty.getValue("site.domain");

            case "gl": return "Escrito por "+ authorName +".\n" +
                    "Audio impulsado por " + ReadProperty.getValue("site.domain");

            case "de": return "Geschrieben von "+ authorName +".\n" +
                    "Audio von threadvoice.net";

            case "tr": return "Yazan "+ authorName;

            case "it": return "Scritto da "+ authorName +".\n" +
                    "Audio alimentato da " + ReadProperty.getValue("site.domain");

            case "ru": return "Автор Иеремия " + authorName + " Аудио от " +
                    ReadProperty.getValue("site.domain");

            default: return "Written by " + authorName + ".\nAudio Powered by " +
                    ReadProperty.getValue("site.domain");
        }
    }

    // randomize which service gets to process the tweet.
    public static String randomizeTTSProvider(String threadContent, String voiceFilename){
        switch (new Random().nextInt(2)){
            case 0: return GoogleTTS.processSpeech(threadContent, voiceFilename);
            case 1: return VoiceRSS.processSpeech(threadContent, voiceFilename);
            default: return GoogleTTS.processSpeech(threadContent, voiceFilename);
        }
    }

}