import twitter4j.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ThreadReader {
    private static final Twitter twitter = TwitterFactory.getSingleton();

    public static List<Status> formatThread(List<Status> statuses) {
        List<Status> statusList = new ArrayList<>();
        statusList.add(statuses.get(0));

        List<Status> filtered = new ArrayList<>();
        long replyIdForNextTweet = statusList.get(0).getId();

        List<Status> upperTweets = readUpperTweets(replyIdForNextTweet, statuses.get(0).getUser().getScreenName());

        for (int i = 1; i < statuses.size(); i++) {
            if (statuses.get(i).getInReplyToStatusId() == replyIdForNextTweet) {
                replyIdForNextTweet = statuses.get(i).getId();
                filtered.add(statuses.get(i));
            }
        }

        filtered.addAll(0, statusList);
        if (upperTweets != null && upperTweets.size() > 0) {
            filtered.addAll(0, upperTweets);
        }

        return filtered;
    }

    public static List<Status> getTweetSinceId(long tweetId) {
        Status status = null;
        try {
            status = twitter.showStatus(tweetId);
        } catch (TwitterException e) {
            e.printStackTrace();

            if (e.getMessage().contains("Rate limit exceeded")){
                System.out.println("TweetSince: Status API Rate Limited. Sleeping for 5 minutes." );
                try {
                    Thread.sleep(300000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

        Paging paging = new Paging();
        paging.setSinceId(tweetId);
        paging.setCount(200);

        List<Status> tweets = new ArrayList<>();

        while (true) {

            List<Status> timeline = null;
            try {
                timeline = twitter.getUserTimeline(status.getUser().getId(), paging);
            } catch (TwitterException e) {
                e.printStackTrace();

                if (e.getMessage().contains("Rate limit exceeded")){
                    System.out.println("User Timeline API Rate Limited. Sleeping for 5 minutes." );
                    try {
                        Thread.sleep(300000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }

            Collections.sort(timeline, new Comparator<Status>() {
                @Override
                public int compare(Status status1, Status status2) {
                    return String.valueOf(status1.getId()).compareTo(String.valueOf(status2.getId()));
                }
            });

            boolean duplicate = false;

            for (Status stat : timeline) {
                if (stat.getId() == paging.getMaxId()) {
                    duplicate = true;
                }
            }

            if (duplicate) {
                timeline.remove(timeline.size() - 1);
            }

            if (timeline.size() == 0) break;

            paging.setMaxId(timeline.get(0).getId());

            tweets.addAll(0, timeline);
        }

        tweets.add(0, status);

        return tweets;
    }

    public static List<Status> readUpperTweets(long lastId, String screenName) {

        Status status = null;
        try {
            status = twitter.showStatus(lastId);
        } catch (TwitterException e) {

            if (e.getMessage().contains("Rate limit exceeded")){
                System.out.println("ReadUpperTweets: Status API Rate Limited. Sleeping for 5 minutes." );
                try {
                    Thread.sleep(300000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            } else if (e.getMessage().contains("The URI requested is invalid or the resource requested")){
                System.out.println("deleted tweet found");
            } else {
                e.printStackTrace();
            }
        }

        long id = 0L;
        List<Status> tweets = new ArrayList<>();

//        tweets.add(status);

        if (status.getInReplyToStatusId() != -1) {
            do {
                id = status.getInReplyToStatusId();
                if (id != -1) {
                    try {
                        status = twitter.showStatus(id);
                    } catch (TwitterException e) {
                        if (e.getMessage().contains("you are not authorized to see this status")) {
                            System.out.println("Protected status: " + id);
                        }
                        else if (e.getMessage().contains("The URI requested is invalid or the resource requested")){
                            System.out.println("deleted tweet " + id);
                        } else {
                            System.out.println("exception tweet: " + id);
                            e.printStackTrace();

                        }
                        break;
                    }
                }

                if (status.getUser().getScreenName().equalsIgnoreCase(screenName)) {
                    tweets.add(status);
                }
            } while (status.getInReplyToStatusId() != -1
                    && status.getUser().getScreenName().equalsIgnoreCase(screenName));
        }

        Collections.reverse(tweets);

        return tweets;
    }
}
