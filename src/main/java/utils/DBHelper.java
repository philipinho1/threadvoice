package utils;

import java.sql.*;

public class DBHelper {
    private static Connection connection;
    private static final String DATABASE = ReadProperty.getValue("mysql.db");

    public static void saveTweet(String username, long mentionId, long threadTweetId, String voiceUrl, String threadTweetUser, String threadTweetText, String language) {
        String sql = "INSERT INTO tweet_records(username, mention_id, thread_tweet_id, voice_url, thread_tweet_user, thread_tweet_text,language) VALUES(?,?,?,?,?,?,?)";
        try {
            connection = DriverManager.getConnection(DATABASE);
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setLong(2, mentionId);
            ps.setLong(3, threadTweetId);
            ps.setString(4, voiceUrl);
            ps.setString(5,threadTweetUser);
            ps.setString(6,threadTweetText);
            ps.setString(7,language);
            ps.executeUpdate();
            connection.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static String getThreadVoiceName(long threadId) {
        StringBuilder voiceName = new StringBuilder();
        try {
            connection = DriverManager.getConnection(DATABASE);
            PreparedStatement ps = connection.prepareStatement("SELECT voice_url FROM tweet_records WHERE thread_tweet_id =" + threadId + ";");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                voiceName.append(rs.getString("voice_url"));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return voiceName.toString();
    }

    public static boolean isThreadExists(long threadId){
        boolean response = false;
        try {
            connection = DriverManager.getConnection(DATABASE);
            PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM tweet_records WHERE thread_tweet_id LIKE '" + threadId + "'");

            try(ResultSet rs = ps.executeQuery()){
                if (rs.next()){
                    boolean found = rs.getBoolean(1);

                    if (found){
                        response = true;
                    } else {
                        response = false;
                    }
                }
            }

            connection.close();
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return response;
    }

}