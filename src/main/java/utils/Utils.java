package utils;

import com.vdurmont.emoji.EmojiParser;

public class Utils {

    public static String cleanThread(String tweets){
        String matchLinks = "(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

        return EmojiParser.removeAllEmojis(tweets.toLowerCase()
                .replaceAll(matchLinks, "")
                .replaceAll("@threavoice audio","")
                .replaceAll("@threadvoice record",""));
    }
}
