CREATE DATABASE IF NOT EXISTS `threadvoice`;
USE `threadvoice`;

CREATE TABLE IF NOT EXISTS `tweet_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `mention_id`  bigint(20) NOT NULL,
  `thread_tweet_id`  bigint(20) DEFAULT NULL,
  `voice_url` varchar(500) DEFAULT NULL,
  `thread_tweet_user` varchar(20) DEFAULT NULL,
  `thread_tweet_text` varchar(500) DEFAULT NULL,
  `thread_language` varchar(300) DEFAULT NULL,
  `time_saved` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY(`mention_id`)
) ENGINE=InnoDB;